<?php

/**
 * Fired during plugin activation
 *
 * @link       www.shelftaught.com/duncan.garde
 * @since      1.0.0
 *
 * @package    Wp_Shelftaught
 * @subpackage Wp_Shelftaught/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_Shelftaught
 * @subpackage Wp_Shelftaught/includes
 * @author     Duncan Garde <duncan@shelftaught.com>
 */
class Wp_Shelftaught_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
