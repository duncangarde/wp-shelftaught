<?php

/**
 * Fired during plugin deactivation
 *
 * @link       www.shelftaught.com/duncan.garde
 * @since      1.0.0
 *
 * @package    Wp_Shelftaught
 * @subpackage Wp_Shelftaught/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wp_Shelftaught
 * @subpackage Wp_Shelftaught/includes
 * @author     Duncan Garde <duncan@shelftaught.com>
 */
class Wp_Shelftaught_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
