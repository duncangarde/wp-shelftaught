<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       www.shelftaught.com/duncan.garde
 * @since      1.0.0
 *
 * @package    Wp_Shelftaught
 * @subpackage Wp_Shelftaught/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Wp_Shelftaught
 * @subpackage Wp_Shelftaught/includes
 * @author     Duncan Garde <duncan@shelftaught.com>
 */
class Wp_Shelftaught_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'wp-shelftaught',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
