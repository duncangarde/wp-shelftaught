<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       www.shelftaught.com/duncan.garde
 * @since      1.0.0
 *
 * @package    Wp_Shelftaught
 * @subpackage Wp_Shelftaught/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
